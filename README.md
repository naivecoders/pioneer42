Pioneer 42
==========

Pioneer 42 is a strategy game where you need to terraform your environment to survive.

Storyline
---------

The year is 3128. Earth is no longer. The last remnants of our civilization is wandering in the space, looking for an habitable planet. But time runs short. A panel of explorers are sent in different locations to create colonies. Conditions of selected planets can be hostile and extreme but thanks to your technology, you can accelerate the environment modification to make it compatible with human life. You civilization is counting on you.

How to run
----------

Coming soon. For now, nothing interesting is happening, and the architecture of the app is massively changing, so I didn't focus on providing a user interface yet to interact with the game. It will come.

If you really interested in running something, try:

`go run p42/pioneer42.go`
