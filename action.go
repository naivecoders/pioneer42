package pioneer42

// Action represent an user interaction with the world
type Action interface {

	//Category return the category of action
	//The category guide which method to call when handling the action
	Category() string
}

type AddResourceAction interface {

	//See Action interface
	Category() string

	//Coordinates return the column and row where to apply the action
	Coordinates() (x, y int)

	// Resoure returns which Resource this action will apply to
	Resource() Resource

	// Amount return the amount of Resource concerned by the action
	Amount() int
}
