package actions

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"bitbucket.org/naivecoders/pioneer42/worlds"
	"errors"
)

// Action categories implemented here
const ActionIDAddResource string = "AddAddResourceAction"

type DefaultAddResourceAction struct {
	category string
	x        int
	y        int
	player   Player
	resource Resource
	amount   int
}

// Implementation of Action
func (ra *DefaultAddResourceAction) Category() string {
	return ra.category
}

// Implementation of AddResourceAction
func (ra *DefaultAddResourceAction) Coordinates() (x, y int) {
	return ra.x, ra.y
}

// Implementation of AddResourceAction
func (ra *DefaultAddResourceAction) Resource() Resource {
	return ra.resource
}

// Implementation of AddResourceAction
func (ra *DefaultAddResourceAction) Amount() int {
	return ra.amount
}

// Return an Action of type AddResourceAction
func AddResource(resource Resource, amount int, x int, y int, player Player) AddResourceAction {
	return &DefaultAddResourceAction{
		category: ActionIDAddResource,
		x:        x,
		y:        y,
		player:   player,
		resource: resource,
		amount:   amount,
	}

}

// Apply returns the world modified by the actions
func Apply(world World, action Action) (World, error) {
	switch cat := action.Category(); cat {
	case ActionIDAddResource:
		return applyAddResource(world, action.(AddResourceAction)), nil
	default:
		return nil, errors.New("Unrecognized action")
	}
}

// applyAddResourceAction returns a World modified by the action
func applyAddResource(world World, action AddResourceAction) World {
	x, y := action.Coordinates()
	return worlds.AddResource(world, x, y, action.Resource(), action.Amount())
}
