package actions

import (
	"bitbucket.org/naivecoders/pioneer42/resources"
	"bitbucket.org/naivecoders/pioneer42/worlds"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAddResourceCreation(t *testing.T) {
	addResource := AddResource(resources.Water, 100, 3, 4, nil)
	assert.NotNil(t, addResource)
	assert.Equal(t, ActionIDAddResource, addResource.Category())
	x, y := addResource.Coordinates()
	assert.Equal(t, 3, x)
	assert.Equal(t, 4, y)
	assert.Equal(t, resources.Water, addResource.Resource())
	assert.Equal(t, 100, addResource.Amount())
}

func TestAddRessourceApply(t *testing.T) {
	world := worlds.NewWithResource("TestWorld", 10, 10, resources.Mineral, 50)
	addResource := AddResource(resources.Water, 100, 3, 4, nil)
	world, err := Apply(world, addResource)
	assert.Nil(t, err)
	assert.Equal(t, 100, world.Plot(3, 4).Stock()["water"])
}
