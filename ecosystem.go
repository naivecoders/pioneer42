package pioneer42

// An ecosystem is a stack of resources and associated amount
type Ecosystem interface {

	// Top returns the resource at the top of the stack of this ecosystem
	Top() Resource

	// Below returns the ecosystem under the top resource
	// may be nil
	Below() Ecosystem

	// Capacity returns the maximum amount of resources
	// the ecosystem can hold
	Capacity() map[string]int

	// Stock returns the amount of each resource this ecosystem has
	Stock() map[string]int
}
