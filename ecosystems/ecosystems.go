package ecosystems

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"bitbucket.org/naivecoders/pioneer42/resources"
)

type DefaultEcosystem struct {
	stock    int
	resource Resource
	below    Ecosystem
}

//Implementation of Ecosystem
func (e *DefaultEcosystem) Top() Resource {
	return e.resource
}

//Implementation of Ecosystem
func (e *DefaultEcosystem) Below() Ecosystem {
	return e.below
}

//Implementation of Ecosystem
func (e *DefaultEcosystem) Capacity() map[string]int {
	if e.below == nil {
		return map[string]int{e.resource.Name(): e.resource.Capacity()}
	}
	return resources.Sum(e.below.Capacity(), map[string]int{e.resource.Name(): e.resource.Capacity()})
}

//Implementation of Ecosystem
func (e *DefaultEcosystem) Stock() map[string]int {
	if e.below == nil {
		return map[string]int{e.resource.Name(): e.stock}
	}
	return resources.Sum(e.below.Stock(), map[string]int{e.resource.Name(): e.stock})
}

//New return a new Ecosystem with the resource and amount defined
// and below an optional existing ecosystem
func New(resource Resource, amount int, below Ecosystem) Ecosystem {
	return &DefaultEcosystem{
		resource: resource,
		stock:    amount,
		below:    below,
	}
}

// Return a new Ecosystem with the content of the passed ecosystem added with the passed amount
func AddResource(ecosystem Ecosystem, resource Resource, amount int) Ecosystem {
	if ecosystem.Top().Name() == resource.Name() {
		sum := amount + ecosystem.Stock()[resource.Name()]
		if sum > ecosystem.Top().Capacity() {
			sum = ecosystem.Top().Capacity()
		}
		return New(resource, sum, ecosystem.Below())
	}
	if ecosystem.Below() != nil {
		return New(ecosystem.Top(), ecosystem.Stock()[ecosystem.Top().Name()], AddResource(ecosystem.Below(), resource, amount))
	}
	return New(ecosystem.Top(), ecosystem.Stock()[ecosystem.Top().Name()], New(resource, amount, nil))
}
