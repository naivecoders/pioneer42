package ecosystems

import (
	"bitbucket.org/naivecoders/pioneer42/resources"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEcosystemCreation(t *testing.T) {
	ecosystem := New(resources.Lichen, 5, nil)
	assert.NotNil(t, ecosystem)
	assert.Equal(t, 5, ecosystem.Stock()["lichen"])
	assert.Equal(t, 10, ecosystem.Capacity()["lichen"])
}

func TestAddResource(t *testing.T) {
	ecosystem := New(resources.Lichen, 5, nil)
	ecosystem = AddResource(ecosystem, resources.Lichen, 3)
	ecosystem = AddResource(ecosystem, resources.Water, 10)
	ecosystem = AddResource(ecosystem, resources.Grass, 50)
	assert.Equal(t, 8, ecosystem.Stock()["lichen"])
	assert.Equal(t, 10, ecosystem.Stock()["water"])
	assert.Equal(t, resources.Water.Capacity(), ecosystem.Capacity()["water"])
}

func TestAddMoreThanCapacity(t *testing.T) {
	lichenCap := resources.Lichen.Capacity()
	ecosystem := New(resources.Lichen, 5, nil)
	ecosystem = AddResource(ecosystem, resources.Lichen, lichenCap+100)
	assert.Equal(t, lichenCap, ecosystem.Stock()["lichen"])
}
