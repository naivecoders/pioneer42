package main

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"bitbucket.org/naivecoders/pioneer42/actions"
	"bitbucket.org/naivecoders/pioneer42/players"
	"bitbucket.org/naivecoders/pioneer42/resources"
	"bitbucket.org/naivecoders/pioneer42/worlds"
	"fmt"
)

func main() {
	player := players.New("LonelyPlayer")
	world := worlds.NewWithResource("A boring place", 10, 10, resources.Mineral, 1000)
	addLichen := actions.AddResource(resources.Lichen, 5, 0, 0, player)
	fmt.Println("Original world")
	printWorld(world)
	fmt.Println("... A bit of life")
	world, _ = actions.Apply(world, addLichen)
	printWorld(world)
	fmt.Println("...After 1 rounds")
	world = world.Evolve()
	printWorld(world)
	fmt.Println("... After 10 rounds")
	for i := 1; i < 10; i++ {
		world = world.Evolve()
	}
	printWorld(world)
	fmt.Println("... More to come. Patience")
}

func printWorld(world World) {
	// TODO
}
