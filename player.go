package pioneer42

// Player represent a game player
type Player interface {

	//Id returns player id
	Id() string
}
