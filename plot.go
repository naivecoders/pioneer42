package pioneer42

// Plot is a land plot, located on a map, with its own ecosystem
type Plot interface {

	//Coordinates return the column and row of the plot in the map
	Coordinates() (x, y int)

	// Stock return the a map of resource id and asociated amount
	// for this land spot
	Stock() map[string]int

	// Ecosystem returns the associated Ecosystem of this plot
	Ecosystem() Ecosystem
}
