package plots

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"bitbucket.org/naivecoders/pioneer42/ecosystems"
)

type DefaultPlot struct {
	x         int
	y         int
	ecosystem Ecosystem
}

// Implementation of Plot
func (p *DefaultPlot) Coordinates() (x, y int) {
	return p.x, p.y
}

// Implementation of Plot
func (p *DefaultPlot) Stock() map[string]int {
	return p.ecosystem.Stock()
}

// Implementation of Plot
func (p *DefaultPlot) Ecosystem() Ecosystem {
	return p.ecosystem
}

func New(x int, y int, ecosystem Ecosystem) Plot {
	return &DefaultPlot{x: x, y: y, ecosystem: ecosystem}
}

func AddResource(plot Plot, resource Resource, amount int) Plot {
	affectedEcosystem := plot.Ecosystem()
	newEcosystem := ecosystems.AddResource(affectedEcosystem, resource, amount)
	x, y := plot.Coordinates()
	return New(x, y, newEcosystem)
}
