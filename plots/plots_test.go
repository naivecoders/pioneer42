package plots

import (
	"bitbucket.org/naivecoders/pioneer42/ecosystems"
	"bitbucket.org/naivecoders/pioneer42/resources"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPlotCreation(t *testing.T) {
	plot_0_0 := New(0, 0, ecosystems.New(resources.Mineral, 1000, nil))
	assert.NotNil(t, plot_0_0)
}

func TestPlotPropagation(t *testing.T) {
	plot_0_0 := New(0, 0, ecosystems.New(resources.Mineral, 1000, nil))
	plot_0_0 = AddResource(plot_0_0, resources.Lichen, 8)
	assert.Equal(t, 1000, plot_0_0.Stock()["mineral"])
	assert.Equal(t, 8, plot_0_0.Stock()["lichen"])
}
