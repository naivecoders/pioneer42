package pioneer42

type Resource interface {

	// Name return the name of the resource
	// It is used as an id to stock in ecosystem
	// It should not collide with existing resources
	Name() string

	// Capacity is the maximum amount
	// a resource can be found in a single ecosystem
	Capacity() int
}
