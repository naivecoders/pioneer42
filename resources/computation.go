package resources

func Multiply(stock map[string]int, coef int) map[string]int {
	result := make(map[string]int)
	if coef != 0 {
		for resource, amount := range stock {
			result[resource] = amount * coef
		}
	}
	return result
}

func Sum(stock1 map[string]int, stock2 map[string]int) map[string]int {
	result := make(map[string]int)
	for resource, stock := range stock1 {
		result[resource] += stock
	}
	for resource, stock := range stock2 {
		result[resource] += stock
	}
	return result
}

func Substraction(stock1 map[string]int, stock2 map[string]int) map[string]int {
	result := make(map[string]int)
	for resource, amount := range stock1 {
		result[resource] = amount
	}
	for resource, amount := range stock2 {
		result[resource] -= amount
	}
	return result
}
