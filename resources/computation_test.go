package resources

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSumResources(t *testing.T) {
	sum := Sum(map[string]int{
		"A": 1,
		"B": 2,
		"C": 1,
	}, map[string]int{
		"A": 2,
		"B": 4,
		"D": 2,
	})
	assert.Equal(t, 3, sum["A"])
	assert.Equal(t, 6, sum["B"])
	assert.Equal(t, 1, sum["C"])
	assert.Equal(t, 2, sum["D"])
}

func TestSumEmptyArgs(t *testing.T) {
	sum := Sum(map[string]int{}, map[string]int{"A": 1})
	assert.Equal(t, 1, sum["A"])
	sum = Sum(map[string]int{"B": 1}, map[string]int{})
	assert.Equal(t, 1, sum["B"])
}

func TestMultiplyResources(t *testing.T) {
	result := Multiply(map[string]int{
		"A": 1,
		"B": 2,
		"C": 3,
	}, 3)
	assert.Equal(t, 3, result["A"])
	assert.Equal(t, 6, result["B"])
	assert.Equal(t, 9, result["C"])
}

func TestMultiplyBy0(t *testing.T) {
	result := Multiply(map[string]int{
		"A": 1,
		"B": 2,
		"C": 3,
	}, 0)
	assert.Empty(t, result)
}

func TestSubstraction(t *testing.T) {
	result := Substraction(map[string]int{
		"A": 1,
		"B": 2,
		"C": 3,
		"D": 4,
	}, map[string]int{
		"A": 1,
		"C": 2,
	})
	assert.Equal(t, 0, result["A"])
	assert.Equal(t, 2, result["B"])
	assert.Equal(t, 1, result["C"])
	assert.Equal(t, 4, result["D"])
}
