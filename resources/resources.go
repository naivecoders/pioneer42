package resources

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"errors"
)

type DefaultResource struct {
	name         string
	consumptions map[string]int
	productions  map[string]int
	max          int
	expansion    int
	progression  int
}

var Lichen = &DefaultResource{
	name: "lichen",
	consumptions: map[string]int{
		"mineral": 1,
	},
	productions: map[string]int{
		"mineral": 1,
		"humus":   1,
		"oxygen":  1,
	},
	max:         10,
	expansion:   1,
	progression: 1,
}

var Mineral = &DefaultResource{
	name:         "mineral",
	consumptions: map[string]int{},
	productions:  map[string]int{},
	max:          10000,
	expansion:    100,
	progression:  0,
}

var Grass = &DefaultResource{
	name: "grass",
	consumptions: map[string]int{
		"humus":  1,
		"oxygen": 1,
		"water":  1,
	},
	productions: map[string]int{
		"humus":  2,
		"oxygen": 3,
	},
	max:         100,
	expansion:   3,
	progression: 3,
}

var Water = &DefaultResource{
	name:         "water",
	consumptions: map[string]int{},
	productions:  map[string]int{},
	max:          10000,
	expansion:    1000,
	progression:  0,
}

// Implementation of Resource
func (r *DefaultResource) Name() string {
	return r.name
}

// Implementation of Resource
func (r *DefaultResource) Capacity() int {
	return r.max
}

// New return the Resource with name passed as argument
// If no resource with such name exists, return an error
func New(name string) (Resource, error) {
	switch name {
	case Lichen.Name():
		return Lichen, nil
	case Mineral.Name():
		return Mineral, nil
	case Grass.Name():
		return Grass, nil
	case Water.Name():
		return Water, nil
	default:
		return nil, errors.New("resource not found")
	}
}
