package resources

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestResourceExistance(t *testing.T) {
	lichen, _ := New("lichen")
	assert.NotNil(t, lichen)
	assert.Equal(t, Lichen.Name(), lichen.Name())
	mineral, _ := New("mineral")
	assert.NotNil(t, mineral)
	assert.Equal(t, Mineral.Name(), mineral.Name())
	water, _ := New("water")
	assert.NotNil(t, water)
	assert.Equal(t, Water.Name(), water.Name())
	grass, _ := New("grass")
	assert.NotNil(t, grass)
	assert.Equal(t, Grass.Name(), grass.Name())

}

func TestUnknownResource(t *testing.T) {
	unknown, err := New("unknown")
	assert.NotNil(t, err)
	assert.Nil(t, unknown)
}

func TestLichenConsumption(t *testing.T) {
	lichen, _ := New("lichen")
	assert.Equal(t, 10, lichen.Capacity())
}
