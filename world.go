package pioneer42

type World interface {

	//Name returns the name of the world
	Name() string

	//Width returns the number of land plot available inside on row of the map
	Width() int

	//Height returns the number of land plot available in one column of the map
	Height() int

	// Evolve computes and return the next state of the world
	Evolve() World

	// Plot return the land plot located at the provided coordinates
	// x defines the column
	// y defines the row
	Plot(x int, y int) Plot

	// Return the all land plots covering the world surfaces
	Plots() [][]Plot
}
