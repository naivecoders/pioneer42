package worlds

import (
	. "bitbucket.org/naivecoders/pioneer42"
	"bitbucket.org/naivecoders/pioneer42/ecosystems"
	"bitbucket.org/naivecoders/pioneer42/plots"
)

//Default implementation of World
type DefaultWorld struct {
	name   string
	width  int
	height int
	plots  [][]Plot
}

// NewWithResource returns a World implementation with a default resource
// the width and height will defines the number of land plots covering the surface
// of the world
// All plots will be initialized with the base resource and the amount defined
func NewWithResource(name string, width int, height int, baseResource Resource, amount int) World {
	plots := createPlots(width, height, baseResource, amount)
	return New(name, width, height, plots)
}

// New returns a World implementation
// the width and height will defines the number of land plots covering the surface
// of the world
// the plots will defines the surface of the world
func New(name string, width int, height int, plots [][]Plot) World {
	return &DefaultWorld{
		name:   name,
		width:  width,
		height: height,
		plots:  plots,
	}
}

// Implementation of World interface
func (w *DefaultWorld) Name() string {
	return w.name
}

// Implementation of World interface
func (w *DefaultWorld) Width() int {
	return w.width
}

// Implementation of World interface
func (w *DefaultWorld) Height() int {
	return w.height
}

// Implementation of World interface
func (w *DefaultWorld) Evolve() World {
	//TODO to implement
	return w
}

// Implementation of World interface
func (w *DefaultWorld) Plot(x int, y int) Plot {
	return w.plots[x][y]
}

// Implementation of World interface
func (w *DefaultWorld) Plots() [][]Plot {
	return w.plots
}

// createPlots return a matrix of land plots with the resource in parameter
func createPlots(width int, height int, baseResource Resource, amount int) [][]Plot {
	worldPlots := make([][]Plot, height)
	for i := range worldPlots {
		worldPlots[i] = make([]Plot, width)
		for j := range worldPlots[i] {
			worldPlots[i][j] = plots.New(i, j, ecosystems.New(baseResource, amount, nil))
		}
	}
	return worldPlots
}

// AddResource returns a world with the new Amount of Resource at the specified location
func AddResource(world World, x int, y int, resource Resource, amount int) World {
	affectedPlot := world.Plot(x, y)
	newPlot := plots.AddResource(affectedPlot, resource, amount)
	plots := world.Plots()
	plots[x][y] = newPlot
	return New(world.Name(), world.Width(), world.Height(), plots)
}
