package worlds

import (
	"bitbucket.org/naivecoders/pioneer42/resources"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWorldCreation(t *testing.T) {
	world := NewWithResource("TestWorld", 4, 3, resources.Water, 1000)
	assert.Equal(t, 4, world.Width())
	assert.Equal(t, 3, world.Height())
	for y := 0; y < world.Width(); y++ {
		for x := 0; x < world.Height(); x++ {
			assert.Equal(t, 1000, world.Plot(x, y).Stock()["water"])
		}
	}
}

func TestAddResource(t *testing.T) {
	world := NewWithResource("TestWorld", 4, 3, resources.Mineral, 200)
	AddResource(world, 2, 2, resources.Mineral, 500)
	AddResource(world, 2, 3, resources.Water, 10)
	assert.Equal(t, 200, world.Plot(1, 1).Stock()["mineral"])
	assert.Equal(t, 700, world.Plot(2, 2).Stock()["mineral"])
	assert.Equal(t, 10, world.Plot(2, 3).Stock()["water"])
}
